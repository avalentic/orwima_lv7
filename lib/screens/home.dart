import 'package:flutter/material.dart';
import 'package:lv7/screens/home_fragments/1.dart';
import 'package:lv7/screens/home_fragments/2.dart';
import 'package:lv7/screens/home_fragments/3.dart';
import 'package:lv7/screens/home_fragments/4.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  TabController _tabController;
  String _fragment2Text = "";
  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff018677),
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Color(0xffb84476),
          tabs: [
            Tab(text: "#1"),
            Tab(text: "#2"),
            Tab(text: "#3"),
            Tab(text: "#4"),
          ],
        ),
        title: Text('LV3'),
        centerTitle: false,
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          fragment1(() => _buttonFunction(), _textEditingController),
          fragment2(_fragment2Text),
          fragment3(),
          fragment4(true),
        ],
      ),
    );
  }

  void _buttonFunction() {
    setState(() {
      _fragment2Text = _textEditingController.text;
      _textEditingController.clear();
      _tabController.animateTo(1);
    });
  }
}
