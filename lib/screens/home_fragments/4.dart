import 'package:flutter/material.dart';

Widget fragment4(bool isImage) => Container(
      color: Colors.white,
      child: Center(
        child: isImage
            ? Image.asset(
                'assets/images/image.jpg',
                height: 250,
                width: 250,
                fit: BoxFit.fill,
              )
            : Text("Hello World!"),
      ),
    );
