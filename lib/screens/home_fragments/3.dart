import 'package:flutter/material.dart';

Widget fragment3() => Container(
  color: Color(0xff018677),
  child: Center(
    child: Text(
      "Hello World!",
      style: TextStyle(
        color: Colors.white,
        fontSize: 20,
      ),
    ),
  ),
);