import 'package:flutter/material.dart';

Widget fragment2(String text) => Container(
  color: Color(0xff3a3b69),
  child: Center(
    child: Text(
      text,
      style: TextStyle(
        color: Colors.white,
        fontSize: 20,
      ),
    ),
  ),
);