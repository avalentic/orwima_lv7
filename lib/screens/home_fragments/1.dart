import 'package:flutter/material.dart';

Widget fragment1(Function buttonFunction, TextEditingController textEditingController) => Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      color: Color(0xff66b2b2),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Container(
                margin: const EdgeInsets.only(right: 32),
                child: TextField(
                  controller: textEditingController,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            FlatButton(
              onPressed: buttonFunction,
              color: Color(0xffd6d7d7),
              child: Text("SUBMIT"),
            )
          ],
        ),
      ),
    );
