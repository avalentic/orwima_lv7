import 'package:flutter/material.dart';
import 'package:lv7/screens/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'LV3',
      theme: ThemeData(
        primaryColor: Color(0xffb84476),
      ),
      home: Home(),
    );
  }
}
